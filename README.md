CMPE 295 Use Case - AWS Lambda function

Getting Started

This program is part of the use case. The program forwards any messages published to the MQTT topic
"vehicles/alerts" to Greengrass devices that are subscribed to the same topic.

Prerequisites

The machine that will run as the Greengrass Core must have the Greengrass image
installed to run the Lambda functions.

Installing

The code can be uploaded or copied-and-pasted into a Lambda function at the
AWS Lambda website.

Deployment

Deployment occurs through the AWS Lambda console.

https://aws.amazon.com/lambda/

Versioning

v1.0

Authors

Sona Bhasin
Kanti Bhat
Johnny Nigh
Monica Parmanand
