import sys

import json

import greengrasssdk

client = greengrasssdk.client('iot-data')

def function_handler(event, context):

    client.publish(topic='vehicles/alerts', payload=json.dumps(event))

    return
